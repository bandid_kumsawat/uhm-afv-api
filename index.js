const http = require('http'),
    path = require('path'),
    methods = require('methods'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    cors = require('cors'),
    passport = require('passport'),
    errorhandler = require('errorhandler'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    morgan = require('morgan'),
    methodover = require('method-override'),
    AFVMQTT = require("./mqtt"),
    multer = require('multer')

var app = express();
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodover());
app.use(errorhandler());
app.use(multer().array())

// mongoose.connect('mongodb://127.0.0.1:27017/afv');
// mongoose.connect('mongodb://admin:deaw1234!@ssmonitors.site/afv', {auth:{authdb:"admin"}});
mongoose.connect('mongodb://admin:8fzLIpeKYS4Dgv5327@localhost:4003/afv', {auth:{authdb:"admin"}});
require('./models/Logs');
require('./models/Devices')
app.use(require('./routes'));

AFVMQTT()

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: err
  }});
});


// finally, let's start our server...
var server = app.listen( process.env.PORT || 4002, function(){
  console.log('Listening on port ' +"http://localhost:"+server.address().port);
});


/**
 * 
 * db.logs.remove( {"device_id":"AFV-TEST-001"});
 * 
 */