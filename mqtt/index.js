const mqtt = require("mqtt")
const config = require("./../config")
var mongoose = require('mongoose');
require('./../models/Logs');
require('./../models/Devices');
var logs = mongoose.model('Logs')
var devices = mongoose.model("Devices")

function CheckValue(payload, topic, device) {
  var sensor = {}
  var compareTopic = "logger/" + device
  if (topic === compareTopic + "/afv/pressure" ){
    sensor.tag = "pressure"
    sensor.value = payload.ps
    sensor.time = payload.ts
    sensor.text = "ค่าแรงดันน้ำ"
    sensor.priority = "Low Low"
    sensor.status = "Message"
  } else if (topic === compareTopic + "/afv/flow/active" ){
    sensor.tag = "flow"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "น้ำเริ่มไหล"
    sensor.priority = "Hight"
    sensor.status = "Active"
  } else if (topic === compareTopic + "/afv/flow/inactive" ){
    sensor.tag = "flow"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "น้ำหยุดไหล"
    sensor.priority = "Hight"
    sensor.status = "Inactive"
  } else if (topic === compareTopic + "/afv/valve/open" ){
    sensor.tag = "valve"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "เปิดวาล์ว"
    sensor.priority = "Hight"
    sensor.status = "Open"
  }else if (topic === compareTopic + "/afv/valve/close" ){
    sensor.tag = "valve"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "ปิดวาล์ว"
    sensor.priority = "Hight"
    sensor.status = "Close"
  } else if (topic === compareTopic + "/afv/valve/open/timeout" ){
    sensor.tag = "valve"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "เปิดวาล์วแต่น้ำไม่ไหล"
    sensor.priority = "Hight Hight"
    sensor.status = "Timeout"
  } else if (topic === compareTopic + "/afv/valve/close/timeout" ){
    sensor.tag = "valve"
    sensor.value = null
    sensor.time = payload.ts
    sensor.text = "ปิดวาล์วแต่น้ำไหลไม่หยุด"
    sensor.priority = "Hight Hight"
    sensor.status = "Timeout"
  } else if (topic === compareTopic + "/afv/volume" ){
    sensor.tag = "volume"
    sensor.value = payload.vo
    sensor.time = payload.ts
    sensor.text = "ปริมาณน้ำทิ้งทั้งหมด"
    sensor.priority = "Low Low"
    sensor.status = "Message"
  }
  return sensor
}

function subscriptTopicAFV () {
  var clientMQTT = mqtt.connect(config.host, config)
  console.log("mqtt connected")
  var topic_list=["logger/+/afv/#"];
  clientMQTT.subscribe(topic_list,{qos:2});

  //handle incoming messages
  clientMQTT.on('message',function(topic, message, payload){
    var slog = JSON.parse(message)
    console.log(slog)
    var device_id = topic.split("/")[1]
    var tlog = CheckValue(slog, topic, device_id)
    var utc = new Date((tlog.time - (3600 * 7)) * 1000)
    var gmt = new Date((tlog.time * 1000))
    // pressure
    // flow
    // valve
    // volume
    var device_update = {}
    if (tlog.tag === "pressure"){
      device_update.device_id = device_id

      device_update.pressure = {}
      // device_update.pressure_time_utc = utc
      // device_update.pressure_time_gmt = gmt
      // device_update.pressure_device_id = device_id
      // device_update.pressure_text = tlog.text
      // device_update.pressure_value = tlog.value
      // device_update.pressure_status = tlog.status
      // device_update.pressure_topic = tlog.topic
      // device_update.pressure_priority = tlog.priority
      // device_update.pressure_payload = tlog.payload
      device_update.pressure.time_utc = utc
      device_update.pressure.time_gmt = gmt
      device_update.pressure.device_id = device_id
      device_update.pressure.text = tlog.text
      device_update.pressure.value = tlog.value
      device_update.pressure.status = tlog.status
      device_update.pressure.topic = topic
      device_update.pressure.priority = tlog.priority
      device_update.pressure.payload = slog

      device_update.last_update_utc = utc
      device_update.last_update_gmt = gmt
    }else if (tlog.tag === "flow"){
      device_update.device_id = device_id

      device_update.flow = {}
      // device_update.flow_time_utc = utc
      // device_update.flow_time_gmt = gmt
      // device_update.flow_device_id = device_id
      // device_update.flow_text = tlog.text
      // device_update.flow_value = tlog.value
      // device_update.flow_status = tlog.status
      // device_update.flow_topic = tlog.topic
      // device_update.flow_priority = tlog.priority
      // device_update.flow_payload = tlog.payload
      device_update.flow.time_utc = utc
      device_update.flow.time_gmt = gmt
      device_update.flow.device_id = device_id
      device_update.flow.text = tlog.text
      device_update.flow.value = tlog.value
      device_update.flow.status = tlog.status
      device_update.flow.topic = topic
      device_update.flow.priority = tlog.priority
      device_update.flow.payload = slog
      
      device_update.last_update_utc = utc
      device_update.last_update_gmt = gmt
    }else if (tlog.tag === "valve"){
      device_update.device_id = device_id

      device_update.valve = {}
      device_update.valve.time_utc = utc
      device_update.valve.time_gmt = gmt
      device_update.valve.device_id = device_id
      device_update.valve.text = tlog.text
      device_update.valve.value = tlog.value
      device_update.valve.status = tlog.status
      device_update.valve.topic = topic
      device_update.valve.priority = tlog.priority
      device_update.valve.payload = slog
      
      device_update.last_update_utc = utc
      device_update.last_update_gmt = gmt
    }else if (tlog.tag === "volume"){
      device_update.device_id = device_id

      // device_update.volume_time_utc = utc
      // device_update.volume_time_gmt = gmt
      // device_update.volume_device_id = device_id
      // device_update.volume_text = tlog.text
      // device_update.volume_value = tlog.value
      // device_update.volume_status = tlog.status
      // device_update.volume_topic = tlog.topic
      // device_update.volume_priority = tlog.priority
      // device_update.volume_payload = tlog.payload

      device_update.volume = {}
      device_update.volume.time_utc = utc
      device_update.volume.time_gmt = gmt
      device_update.volume.device_id = device_id
      device_update.volume.text = tlog.text
      device_update.volume.value = tlog.value
      device_update.volume.status = tlog.status
      device_update.volume.topic = topic
      device_update.volume.priority = tlog.priority
      device_update.volume.payload = slog
      
      device_update.last_update_utc = utc
      device_update.last_update_gmt = gmt
    }
    devices.find({device_id: device_id}).then((result) => {
      if (result.length > 0){
        devices.findOneAndUpdate({device_id: device_id}, device_update, {new: true}, (err, doc) => {
          if (err) console.log("cannot update devices.")
          var log = new logs()
          log.time_utc = utc
          log.time_gmt = gmt
          log.device_id = device_id
          log.value = tlog.value
          log.text = tlog.text
          log.priority = tlog.priority
          log.status = tlog.status
          log.topic = topic
          log.payload = slog
          log.save()
        }).catch((error) => {
          if (error) console.log("cannot update devices.")
        })
      }else {
        var devices_new = new devices()
        devices_new.device_id = device_id
        devices_new.save().then((result) => {
          console.log("new record")
          devices.findOneAndUpdate({device_id: device_id}, device_update, {new: true}, (err, doc) => {
            if (err) console.log("cannot update devices.")
            var log = new logs()
            log.time_utc = utc
            log.time_gmt = gmt
            log.device_id = device_id
            log.value = tlog.value
            log.text = tlog.text
            log.priority = tlog.priority
            log.status = tlog.status
            log.topic = topic
            log.payload = slog
            log.save()
          }).catch((error) => {
            if (error) console.log("cannot update devices.")
          })
        }).catch((error) => {
          if (error) console.log("cannot save new devices.")
        })
      }
    })

    // devices.find({device_id: device_id}).then((result) => {
    //   if (result.length > 0){
    //     // update 
    //     devices.findOneAndUpdate({}, {}, {new: true})
    //   } else {
    //     console.log('save new device')
    //   }
    // })


    // var log = new logs()
    // log.time_utc = utc
    // log.time_gmt = gmt
    // log.device_id = device_id
    // log.value = tlog.value
    // log.text = tlog.text
    // log.priority = tlog.priority
    // log.status = tlog.status
    // log.topic = topic
    // log.save()
  });
}

module.exports = subscriptTopicAFV;

// payload param
// {
//   "cmd":"publish",
//   "retain":false,
//   "qos":0,
//   "dup":false,
//   "length":47,
//   "topic":"afv/AFV-TEST-001/valve/close",
//   "payload":{
//     "type":"Buffer",
//     "data":[123,34,116,115,34,58,49,54,50,50,49,50,56,54,54,53,125]
//   }
// }   
