var router = require('express').Router();
var mongoose = require('mongoose');
var logs = mongoose.model('Logs')

router.post("/log", async (req, res, next) => {
  var offset = req.body.offset
  var limit = req.body.limit
  var sort = req.body.sort
  var query = {
    time_utc: {
      $gte: new Date(req.body.start_time),
      $lte: new Date(req.body.stop_time),
    },
    device_id: req.body.device_id
  }
  var count = await logs.find(query).count()
  var result = await logs.find(query).skip(Number(offset)).limit(Number(limit)).sort({"time_utc": sort})
  return res.json({
    message: result,
    length: count
  })
})

module.exports = router;