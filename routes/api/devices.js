var router = require('express').Router();
var mongoose = require('mongoose');
var devices = mongoose.model('Devices')

router.post("/", async (req, res, next) => {
  var query = {
    device_id: req.body.device_id
  }
  var result = await devices.find(query)
  return res.json({
    result: result,
  })
})


module.exports = router;