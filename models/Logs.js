var mongoose = require('mongoose');

var LogsSchema = new mongoose.Schema({
  time_utc: Date,
  time_gmt: Date,
  device_id: String,
  text: String,
  value: String,
  status: String,
  topic: String,
  priority: String,
  payload: Object
});

mongoose.model('Logs', LogsSchema);