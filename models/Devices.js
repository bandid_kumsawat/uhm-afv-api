var mongoose = require('mongoose');

var DevicesSchema = new mongoose.Schema({
  device_id: String,

  pressure: Object,
  valve: Object,
  flow: Object,
  volume: Object,

  // pressure_time_utc: Date,
  // pressure_time_gmt: Date,
  // pressure_device_id: String,
  // pressure_text: String,
  // pressure_value: String,
  // pressure_status: String,
  // pressure_topic: String,
  // pressure_priority: String,
  // pressure_payload: Object,

  // valve_time_utc: Date,
  // valve_time_gmt: Date,
  // valve_device_id: String,
  // valve_text: String,
  // valve_value: String,
  // valve_status: String,
  // valve_topic: String,
  // valve_priority: String,
  // valve_payload: Object,

  // flow_time_utc: Date,
  // flow_time_gmt: Date,
  // flow_device_id: String,
  // flow_text: String,
  // flow_value: String,
  // flow_status: String,
  // flow_topic: String,
  // flow_priority: String,
  // flow_payload: Object,


  // volume_time_utc: Date,
  // volume_time_gmt: Date,
  // volume_device_id: String,
  // volume_text: String,
  // volume_value: String,
  // volume_status: String,
  // volume_topic: String,
  // volume_priority: String,
  // volume_payload: Object,

  last_update_utc: Date,
  last_update_gmt: Date,
});

mongoose.model('Devices', DevicesSchema);